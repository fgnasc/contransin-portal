const apiRoot = 'localhost:8080';

export const environment = {
  production: false,
  apiRoot,
  apiUrl: apiRoot + "/contransin/api/v1",
};
