import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './shared/pages/page-not-found/page-not-found.component';

const routes: Routes = [
  { 
    path: 'auth',
    loadChildren: () => import('./pages/public/public.module').then(m => m.PublicModule) 
  },
  { 
    path: '',
    loadChildren: () => import('./pages/private/private.module').then(m => m.PrivateModule) 
  },
  {
    path: '404',
    component: PageNotFoundComponent 
  },
  {
    path: '**',
    redirectTo: '404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
