import { NgModule } from '@angular/core';
import { PrivateRoutingModule } from './private-routing.module';
import { PrivateComponent } from './private.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [PrivateComponent, HomeComponent],
  imports: [
    SharedModule,
    PrivateRoutingModule
  ]
})
export class PrivateModule { }
