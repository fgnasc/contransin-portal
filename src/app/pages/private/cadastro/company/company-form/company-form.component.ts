import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { errorHandler } from 'src/app/shared/helpers/error-handler';
import { Column } from 'src/app/shared/layout/grid/grid.component';
import { CompanyService } from 'src/app/shared/services/company.service';


export class BankAccount {
  bankAccountType: string;
  bankName: string;
  agency: string;
  checkingAccount: string;
};

export class ProviderCompany {
  externalPresence: boolean;
  internalClassroom: boolean;
  mobile: boolean;
};

@Component({
  selector: 'app-company-form',
  templateUrl: './company-form.component.html',
  styleUrls: ['./company-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CompanyFormComponent implements OnInit {

  form: FormGroup;
  cnpjSearch: FormGroup;
  stepTwo: boolean = false;
  sucursal: boolean = false;
  columns: Column[] = [
    { name: 'Razão Social', field: 'companyName', order: null },
    { name: 'Nome Fantasia', field: 'fantasyName', order: null },
    { name: 'CNPJ', field: 'cnpj', order: null, mask: '00.000.000/0000-00' },
  ];
  sucursalList: any = [];

  constructor(
    private fb: FormBuilder,
    private companyService: CompanyService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private router: Router,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<CompanyFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    ) { }

  ngOnInit(): void {
    if(this.data){
      this.sucursal = this.data.sucursal;
    }

    this.cnpjSearch = this.fb.group({
      cnpj: ['', [Validators.required]]
    })

    this.form = this.fb.group({
      companyStatus: ['',[Validators.required]],
      cnpj: ['',[Validators.required]],
      companyName: ['',[Validators.required]],
      fantasyName: ['',[Validators.required]],
      stateRegistration: ['',[Validators.required]],
      municipalRegistration: ['',[Validators.required]],
      phone: ['',[Validators.required]],
      email: ['',[Validators.required]],
      isClient: [false],
      isProvider: [false],
      isServiceStation: [false],
      companyType : ['',[Validators.required]],
      companyAddress: this.fb.group({
        street: ['',[Validators.required]],
        country: ['',[Validators.required]],
        complement: [''],
        city: ['',[Validators.required]],
        state: ['',[Validators.required]],
        zipCode: ['',[Validators.required]],
        number: ['',[Validators.required]],
        neighborhood: ['',[Validators.required]],
      }),
      bankAccount: this.fb.group({
        bankAccountType: [0,[Validators.required]],
        bankName: ['',[Validators.required]],
        agency: [''],
        checkingAccount: ['',[Validators.required]]
      }),
      providerCompany: this.fb.group({
        externalPresence: [false],
        internalClassroom: [false],
        mobile: [false],
        vehicleSegment: [false],
        motorcycleSegment: [false],
        truckSegment: [false]
      })
    })
  }

  get f() { return this.form.controls; }

  async save(){
    if(this.sucursal){
      this.dialogRef.close({ dados: this.form.value })
    }else{
      try {
        this.spinner.show();
        const result:any = await this.companyService.add(this.form.value);
        const id = result.body.data.id;
        
        //Percorrer Sucursal e Salvar
        this.sucursalList.map(async (suc)=>{
          try{
            const obj = {
              "parentCompanyId": id,
              "status": 0
            }
            suc.branchCompany = obj;
            const resultSucursal = await this.companyService.add(suc);
          }catch(error){
            this.toastr.error(errorHandler(error), 'Falha na autenticação!');
          }
        })
        //Fim Cadastro Sucursal 


        this.router.navigate(['/cadastro/empresas']);
      } catch (error) {
        this.toastr.error(errorHandler(error), 'Falha na autenticação!');
      } finally {
        this.spinner.hide();
      }
    }
  }
  

  resetForm(): void{
    this.form.reset()
  }

  async searchCNPJ(){
    try {
      this.spinner.show();
      const result = (await this.companyService.getCNPJ(this.cnpjSearch.value.cnpj)).body;
      if(!result.bankAccount){
        result.bankAccount = new BankAccount();
      }

      if(!result.providerCompany){
        result.providerCompany = new BankAccount();
      }
      this.form.patchValue(result);
      this.stepTwo = true;
    } catch (error) {
      console.log(error);
      this.toastr.error(errorHandler(error), 'Falha na autenticação!');
    } finally {
      this.spinner.hide();
    }
  }

  openModal(){
    const dialogRef = this.dialog.open(CompanyFormComponent, {
      width: '90%',
      height: '90%',
      data: {
        sucursal: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result.dados){
        this.sucursalList.push(result.dados);
      }
    });
  }

}
