import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Column } from 'src/app/shared/layout/grid/grid.component';
import { CompanyService } from 'src/app/shared/services/company.service';
import { errorHandler } from 'src/app/shared/helpers/error-handler';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss'],
})
export class CompanyListComponent implements OnInit {
  columns: Column[] = [
    { name: 'Razão Social', field: 'companyName', order: null },
    { name: 'Nome Fantasia', field: 'fantasyName', order: null },
    { name: 'CNPJ', field: 'cnpj', order: null, mask: '00.000.000/0000-00' },
  ];
  result: any = [];

  constructor(
    private companyService: CompanyService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getAll();
  }

  async getAll() {
    try {
      this.spinner.show();
      this.result = (await this.companyService.getAll()).body;
    } catch (error) {
      this.toastr.error(errorHandler(error));
    } finally {    
      this.spinner.hide();
    }
  }

  newCompany($e){
    this.router.navigate(['/cadastro/empresas/novo']);
  }
}
