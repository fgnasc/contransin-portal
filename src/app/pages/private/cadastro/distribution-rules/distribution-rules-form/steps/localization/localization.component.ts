import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { IbgeService, RegionTree } from 'src/app/shared/services/ibge.service';

@Component({
  selector: 'app-localization',
  templateUrl: './localization.component.html',
  styleUrls: ['./localization.component.scss'],
})
export class LocalizationComponent implements OnInit, OnChanges {
  @Input() form: FormGroup;
  regionTree: RegionTree[] = [];
  stateList: RegionTree[] = [];
  cityList: RegionTree[] = [];
  mesoregionList: RegionTree[] = [];
  microregionList: RegionTree[] = [];

  constructor(private ibgeService: IbgeService) {}

  async ngOnInit() {
    this.regionTree = await this.ibgeService.getTreeRegions();
  }

  ngOnChanges(changes: SimpleChanges) {}

  loadChildRegionLevel(
    node: RegionTree,
    regionLevelToLoad: 'state' | 'city' | 'mesoregion' | 'microregion'
  ) {
    console.log(node);
    const arr = ['state', 'city', 'mesoregion', 'microregion'];
    const foundIndex = arr.findIndex(x => x === regionLevelToLoad);
    arr.forEach((x, i) => {
      if (i >= foundIndex) {
        this.form.controls[regionLevelToLoad].setValue(null);
        this[x + 'List'] = i === foundIndex ? node.children : [];  
      }
    });
  }
}
