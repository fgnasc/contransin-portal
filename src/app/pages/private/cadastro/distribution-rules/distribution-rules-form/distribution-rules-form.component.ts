import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { throwIfEmpty } from 'rxjs/operators';
import { Column } from 'src/app/shared/layout/grid/grid.component';
import { IbgeService } from 'src/app/shared/services/ibge.service';

type InspectionType = {
  label: string;
  icon: string;
  value: string;
  selected?: boolean;
};

@Component({
  selector: 'app-distribution-rules-form',
  templateUrl: './distribution-rules-form.component.html',
  styleUrls: ['./distribution-rules-form.component.scss'],
})
export class DistributionRulesFormComponent implements OnInit {
  readonly inspectionTypes = [
    {
      label: 'Presencial Interno',
      icon: 'directions_car',
      value: 'presencial',
      selected: false,
    },
    {
      label: 'Presencial Externo',
      icon: 'home',
      value: 'presencial',
      selected: false,
    },
    { label: 'Remoto', icon: 'phone_iphone', value: 'remoto', selected: false },
  ];
  readonly vehicleSegement = [
    { label: 'Leve', icon: 'directions_car', value: '1', selected: false },
    { label: 'Pesado', icon: 'local_shipping', value: '2', selected: false },
    { label: 'Motoclicleta', icon: 'two_wheeler', value: '3', selected: false },
  ];
  readonly providersList = [
    {
      name: 'Planetun',
      document: '12345678901234',
      presencialInterno: true,
      presencialExterno: true,
      remoto: true,
    },
    {
      name: 'Gato 123',
      document: '12345678901234',
      presencialInterno: true,
      presencialExterno: false,
      remoto: false,
    },
    {
      name: 'Zezinho Seguradora',
      document: '12345678901234',
      presencialInterno: true,
      presencialExterno: true,
      remoto: false,
    },
    {
      name: 'Allan Seguros',
      document: '12345678901234',
      presencialInterno: false,
      presencialExterno: false,
      remoto: true,
    },
  ];
  readonly providersListColumns: Column[] = [
    { field: 'name', name: 'Nome' },
    { field: 'document', name: 'CNPJ' },
    { field: 'presencialInterno', name: 'Presencial Interno' },
    { field: 'presencialExterno', name: 'Presencial Externo' },
    { field: 'remoto', name: 'Remoto' },
  ];

  form: FormGroup = this.fb.group({
    inspectionType: [[], Validators.required],
    providers: [[], Validators.required],
    vehicleSegement: [[], Validators.required],
    quantityMobile: null,
    quantityPresentialInternal: null,
    quantityPresentialExternal: null,
    localization: this.fb.group({
      region: null,
      state: null,
      city: null,
      mesoregion: null,
      microregion: null,
    }),
  });

  constructor(private fb: FormBuilder) {}

  async ngOnInit() {
    this.form.controls.providers.valueChanges.subscribe((x) => console.log(x));
  }

  toggleInspectionType(inspectionType: InspectionType) {
    const foundIndex = this.form.controls.inspectionType.value.findIndex(
      (x) => x === inspectionType.value
    );
    inspectionType.selected = foundIndex === -1;
    if (inspectionType.selected) {
      this.form.controls.inspectionType.value.push(inspectionType.value);
    } else {
      this.form.controls.inspectionType.value.splice(foundIndex, 1);
    }
  }


  toggleVehicleSegment(vehicleSegement: InspectionType) {
    const foundIndex = this.form.controls.inspectionType.value.findIndex(
      (x) => x === vehicleSegement.value
    );
    vehicleSegement.selected = foundIndex === -1;
    if (vehicleSegement.selected) {
      this.form.controls.vehicleSegement.value.push(vehicleSegement.value);
    } else {
      this.form.controls.vehicleSegement.value.splice(foundIndex, 1);
    }
  }

  save() {}
}
