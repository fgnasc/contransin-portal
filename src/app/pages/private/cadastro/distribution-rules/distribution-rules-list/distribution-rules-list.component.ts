import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Column } from 'src/app/shared/layout/grid/grid.component';
import { CompanyService } from 'src/app/shared/services/company.service';

@Component({
  templateUrl: './distribution-rules-list.component.html',
  styleUrls: ['./distribution-rules-list.component.scss']
})
export class DistributionRulesListComponent implements OnInit {

  columns: Column[] = [
    { name: 'Nome', field: 'companyName', order: null },
    // { name: 'Nome Fantasia', field: 'fantasyName', order: null },
    { name: 'CNPJ', field: 'cpnj', order: null, mask: '00.000.000/0000-00' },
  ];
  data: any = [];

  constructor(
    private companyService: CompanyService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    public router: Router,
    public route: ActivatedRoute
  ) {}

  ngOnInit(): void {
  }

  navigateToNew() {
    this.router.navigate(['./novo'], { relativeTo: this.route });
  }

}
