import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DistributionRulesListComponent } from './distribution-rules/distribution-rules-list/distribution-rules-list.component';
import { DistributionRulesFormComponent } from './distribution-rules/distribution-rules-form/distribution-rules-form.component';
import { CompanyListComponent } from './company/company-list/company-list.component';
import { CompanyFormComponent } from './company/company-form/company-form.component';


const routes: Routes = [
  { path: 'empresas', component: CompanyListComponent },
  { path: 'empresas/novo', component: CompanyFormComponent },
  { path: 'regras-de-distribuicao', component: DistributionRulesListComponent },
  { path: 'regras-de-distribuicao/novo', component: DistributionRulesFormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
