import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyRoutingModule } from './company-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CompanyService } from 'src/app/shared/services/company.service';
import { DistributionRulesListComponent } from './distribution-rules/distribution-rules-list/distribution-rules-list.component';
import { DistributionRulesFormComponent } from './distribution-rules/distribution-rules-form/distribution-rules-form.component';
import { CompanyListComponent } from './company/company-list/company-list.component';
import { CompanyFormComponent } from './company/company-form/company-form.component';

import { LocalizationComponent } from './distribution-rules/distribution-rules-form/steps/localization/localization.component';

@NgModule({
  declarations: [
    CompanyListComponent,
    CompanyFormComponent,
    DistributionRulesListComponent,
    DistributionRulesFormComponent,
    LocalizationComponent,
  ],
  imports: [
    CommonModule,
    CompanyRoutingModule,
    SharedModule,
  ],
  entryComponents: [
    CompanyFormComponent
  ],
  providers: [CompanyService],
})
export class CompanyModule {}
