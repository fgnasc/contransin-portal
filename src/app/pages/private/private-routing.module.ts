import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PrivateComponent } from './private.component';

const routes: Routes = [
  {
    path: '',
    data: { title: 'Início' },
    component: PrivateComponent,
    children: [
      { path: 'homeasdf', data: { title: 'ASDF' }, component: HomeComponent },
      { path: 'home', data: { title: 'Home' }, component: HomeComponent },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: '**', redirectTo: '404' },
      {
        path: 'cadastro',
        loadChildren: () =>
          import('./cadastro/company.module').then((m) => m.CompanyModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivateRoutingModule {}
