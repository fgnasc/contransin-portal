import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-full-page-wrapper',
  templateUrl: './full-page-wrapper.component.html',
  styleUrls: ['./full-page-wrapper.component.scss']
})
export class FullPageWrapperComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
