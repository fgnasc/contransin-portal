import { Component, OnInit, ChangeDetectionStrategy, Input, forwardRef, OnChanges, SimpleChanges } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Column } from 'src/app/shared/layout/grid/grid.component';

@Component({
  selector: 'app-selectable-table',
  templateUrl: './selectable-table.component.html',
  styleUrls: ['./selectable-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectableTableComponent),
      multi: true,
    },
  ],
})
export class SelectableTableComponent implements OnInit, ControlValueAccessor, OnChanges {

  @Input() public multiple: boolean = false;
  @Input() public data: any[] = [];
  @Input() public columns: Column[] = [];
  @Input() public pageSize: number = 10;
  @Input() public bindProperty: string = null;
  page: number = 1;
  total: number = 20;

  _data: any[] = []; 

  selected: any | any[] = null;

  _value: any = null;

  get value() {
    return this._value;
  }
  set value(value: any) {
    this._value = value;
    this.onChange(value);
    this.onTouched();
  }

  constructor() { }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.data) {
      this._data = JSON.parse(JSON.stringify(this.data));
    }
    if (changes.multiple) {
      this.selected = changes.multiple.currentValue ? [] : null;
    } 
  }

  getVal(line: any) {
    return this.bindProperty ? line[this.bindProperty] : line; 
  }

  toggleLine(line: any) {
    line.$$selected = !line.$$selected;
    if (this.multiple) {
      const index = (this.selected as any[]).findIndex(x => this.getVal(x) === this.getVal(line));
      if (index > -1) {
        this.selected.splice(index, 1);
      } else {
        this.selected.push(this.getVal(line));
      }
    } else {
      this.selected = (this.selected === line) ? null : line;
    }
    this.writeValue(this.selected);
  }

  writeValue(value): void {
    if (this.multiple) {
      if (this.selected instanceof Array) {
        value = null;
      }
      this.selected === value || [];
    } else {
      this.selected = value || null;
    }
    this.value = this.selected;
  }
  // Allows Angular to register a function to call when the model (rating) changes.
  // Save the function as a property to call later here.
  registerOnChange(fn: (value: Date) => void): void {
    this.onChange = fn;
  }
  // Allows Angular to register a function to call when the input has been touched.
  // Save the function as a property to call later here.
  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
  // Allows Angular to disable the input.
  setDisabledState(isDisabled: boolean): void {}

  onChange = (value: Date) => {};
  onTouched = () => {};

}
