import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  path: string[];
  routerLinks: { title: string, href: string }[];

  constructor(private router: Router) { } 

  ngOnInit(): void {
    this.updateRouterLinks();
    this.router.events.subscribe(x => this.updateRouterLinks());
  }

  updateRouterLinks() {
    this.path = window.location.pathname.split('/');
      this.routerLinks = this.path.map((title, i, arr) => {
        const href = arr.filter((x, j) => j <= i).join('/');
        if (i === 0) {
          title = 'Início'
        }
        return { title: title.replace(/-/g, ' '), href };
      });
  }

}
