import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MenuItem, menuItems } from '../../const/menu-items';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarComponent implements OnInit {

  showSubmenu: boolean = true;
  items: MenuItem[] = menuItems;
  openedItem: MenuItem | { label: string, icon: string, profile: boolean };
  user: any = null;
  openedSubmenuMobile: boolean = false;
  isEnableEditUser: boolean = false;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.authService.user$.subscribe(user => this.user = user);
  }

  closeAll() {
    this.recursivellyCloseItem(this.items) 
    this.openedItem = null;
  }

  private recursivellyCloseItem(items: MenuItem[]) {
    if (!items?.length) {
      return;
    }
    items.forEach(item => {
      item.opened = false;
      this.recursivellyCloseItem(item.subitems);
    })
  }

  openItem(item: MenuItem) {
    this.closeAll();
    do {
      item.opened = true;
      this.openedItem = item;
      item = item.parent;
    } while(item);
  }

  toggleSubmenuMobile() {
    this.openedSubmenuMobile = !this.openedSubmenuMobile;
  }

  openProfile() {
    this.closeAll();
    this.openedItem = {
      icon: 'account_circle',
      label: 'Meu Perfil',
      profile: true
    };
  }

  backMenuItem() {
    if ((this.openedItem as MenuItem)?.parent) {
      this.openItem((this.openedItem as MenuItem).parent);
    } else {
      this.closeAll();
    }
  }

  async logout() {
    await this.authService.logout();
  }

  async toggleEditUser() {
    this.isEnableEditUser = this.isEnableEditUser;
  }

}
