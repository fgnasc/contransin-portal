import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChange,
  SimpleChanges,
  TemplateRef,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

export type Column = {
  name: string;
  field?: string;
  order?: 1 | -1 | null;
  mask?: string;
};

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss'],
})
export class GridComponent implements OnInit, OnChanges {
  @Input() public columns: Column[] = [];
  @Input() public data: any[] = [];
  @Input() public pageSize: number = 10;
  @Input() public btnLabel: string = null;
  @Input() public optionsTemplate: TemplateRef<any> = null;
  @Output() public btnClick: EventEmitter<void> = new EventEmitter<void>();
  page: number = 1;
  total: number = 20;
  public filteredData$: Subject<any[]> = new Subject<any[]>();
  searchControl: FormControl = new FormControl();

  constructor() {}

  ngOnInit(): void {
    this.searchControl.valueChanges
      .pipe(distinctUntilChanged(), debounceTime(500))
      .subscribe(() => this.updateFilters());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.data) {
      this.updateFilters();
    }
  }

  updateFilters() {
    let data = this.data;

    if (this.searchControl.value) {
      data = this.data.filter((value) => 
        JSON.stringify(value)
          .toLowerCase()
          .indexOf(this.searchControl.value.toLowerCase()) !== -1
      );
    }

    const column = this.columns.find((x) => x.order);
    if (column) {
      data.sort(function (a, b) {
        if (a[column.field] > b[column.field]) return column.order;
        else if (a[column.field] < b[column.field]) return -column.order;
        else return 0;
      });
    }
    this.clearPagination();

    this.filteredData$.next(data);
  }

  clearPagination($event?) {
    this.page = 1;
  }

  sort(column: Column) {
    this.columns.filter((x) => x !== column).forEach((x) => (x.order = null));
    column.order = column.order === 1 ? -1 : 1;
    this.updateFilters();
  }
}
