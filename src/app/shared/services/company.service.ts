import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private baseUrl = `${environment.apiUrl}/company`;
  private headers = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(
    private http: HttpClient
  ) { }

  getAll(){
    return this.http
      .get<any>(this.baseUrl + '/ListActiveCompanies',  { headers: this.headers, observe: 'response' })
      .toPromise();
  }
  
  getCNPJ(value){
    return this.http
      .get<any>(this.baseUrl + '/CompanyByCnpjRFB/' + value,  { headers: this.headers, observe: 'response' })
      .pipe(
        tap(res => {
          
        })
      ).toPromise();
  }

  add(body){
    return this.http
      .post<any>(this.baseUrl + '/RegisterNewCompany', body, { headers: this.headers, observe: 'response' })
      .pipe(
        tap(res => {
        })
      ).toPromise();
  }
}
