import { Injectable } from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { tap } from "rxjs/operators";
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

const jwt = new JwtHelperService();
@Injectable({
  providedIn: "root",
})
export class AuthService {

  private readonly userSubject = new BehaviorSubject<any>(null);
  private baseUrl = `${environment.apiUrl}/auth`;
  private headers = new HttpHeaders({ "Content-Type": "application/json" });
 
  private get tokenKey(): string { return 'cp_tkn_x'; }
  private get userKey(): string { return 'cp_usr_x'; }

  get user(): any {
    return JSON.parse(window.localStorage.getItem(this.userKey));
  }

  set user(user) {
    if (user?.claims) {
      Object.entries(user.claims).forEach(x => user[x[0]] = x[1]);
    }
    window.localStorage.setItem(this.userKey, JSON.stringify(user));
    this.userSubject.next(user);
  }

  get token() {
    return window.localStorage.getItem(this.tokenKey);
  }
  set token(token: string) {
    window.localStorage.setItem(this.tokenKey, token);
  }

  get user$(): BehaviorSubject<any> {
    return this.userSubject;
  }

  constructor(
    private http: HttpClient, 
    private router: Router
  ) {
    this.decodeAndNotify();
  }

  authenticate(body) {
    return this.http
      .post<any>(this.baseUrl + '/LoginUser', body, { headers: this.headers, observe: 'response' })
      .pipe(
        tap(res => {
          console.log(res);
          this.token = res.body.data.accessToken;
          this.user = res.body.data.userToken;
        })
      ).toPromise();
  }

  hasToken() {
    return !!this.token;
  }

  isLogged() {
    const isExpired = this.isExpired();
    const token = this.hasToken();
    if (!token || isExpired) {
      console.warn(!token ? `Usuário não autenticado.` : `Token expirado, redirecionando para login...`);
      this.removeUser();
      return false;
    }
    return token && !isExpired;
  }

  logout() {
    window.localStorage.clear();
    this.removeUser();
    this.router.navigate(["/auth/login"]);
  }

  private decodeAndNotify() {
    const user = JSON.parse(window.localStorage.getItem(this.userKey));
    if (!user) {
      // const token = this.getDecodedToken();
    //   this.getUserById(token.id).subscribe(user => {
    //     this.setUser(user);
    //   });
    } else this.user = user;
  }

  removeUser() {
    window.localStorage.removeItem(this.userKey);
    this.userSubject.next(null);
  }

  getDecodedToken() {
    return jwt.decodeToken(this.token);
  }

  isExpired() {
    return jwt.isTokenExpired(this.token);
  }

  removeToken() {
    window.localStorage.removeItem(this.tokenKey);
  }
}
