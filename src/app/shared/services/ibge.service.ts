import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

export interface Region {
  value: number;
  label: string;
}
export interface RegionTree extends Region {
  children: (Region | RegionTree)[];
}

@Injectable({
  providedIn: 'root'
})
export class IbgeService {

  private baseUrl = `${environment.apiUrl}/ibge`;
  private headers = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(
    private http: HttpClient
  ) { }

  static treeRegionsCached: RegionTree[] = null;

  async getTreeRegions(): Promise<RegionTree[]> {

    if (IbgeService.treeRegionsCached) {
      return IbgeService.treeRegionsCached;
    }
    IbgeService.treeRegionsCached = (
      await this.http
        .get<RegionTree[]>(this.baseUrl + '/TreeRegions', { headers: this.headers, observe: 'response' })
        .toPromise()
    ).body;

    return IbgeService.treeRegionsCached;
  }

}
