type Params = {
  label: string;
  route?: string;
  icon?: string;
  subitems?: Params[];
  parent?: MenuItem | null;
};

export class MenuItem {
  public label: string;
  public route: string;
  public icon: string;
  public opened: boolean = false;
  public active: boolean = false;
  public subitems?: MenuItem[] = null;
  public readonly parent: MenuItem | null;

  constructor(params: Params) {
    this.route = params?.route || null;
    this.icon = params?.icon || null;
    this.label = params?.label || null;
    if (params?.subitems?.length) {
      this.subitems = params.subitems.map(
        (x) => new MenuItem({ ...x, parent: this })
      );
    }
    this.parent = params?.parent || null;
  }

  addSubitem(...subitems: MenuItem[]) {
    this.subitems.push(...subitems);
  }
}

export const menuItems: MenuItem[] = [
  new MenuItem({ label: 'Empresa', icon: 'list' }),
  new MenuItem({ label: 'Portal Prestadores', icon: 'group' }),
  new MenuItem({ label: 'Operações', icon: 'build' }),
  new MenuItem({ label: 'Relatórios', icon: 'assignment' }),
  new MenuItem({ label: 'Financeiro', icon: 'attach_money' }),
  new MenuItem({ label: 'Regulação Digital', icon: 'menu_book' }),
];
