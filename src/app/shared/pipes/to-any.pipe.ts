import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toAny'
})
export class ToAnyPipe implements PipeTransform {

  transform(value: unknown): any {
    return value as any;
  }

}
