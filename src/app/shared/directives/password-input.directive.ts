import { Directive, ElementRef, ViewEncapsulation } from "@angular/core";

@Directive({
  selector: "[appPasswordInput]",
})
export class PasswordInputDirective {
  private _shown = false;

  constructor(private el: ElementRef) {
    this.setup();
  }
  setup() {
    const parent = this.el.nativeElement.parentNode;
    const div = document.createElement("div"),
      svgElem = document.createElementNS("http://www.w3.org/2000/svg", "svg"),
      useElem = document.createElementNS("http://www.w3.org/2000/svg", "use");

    useElem.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "assets/icons.svg#eye");

    svgElem.appendChild(useElem);

    div.style.position = "relative"

    svgElem.classList.add("show-password");

    svgElem.style.bottom = "0.5rem";
    svgElem.style.fill = "white";
    svgElem.style.height = "1.5em";
    svgElem.style.margin = "0";
    svgElem.style.position = "absolute";
    svgElem.style.right = "5px";
    svgElem.style.stroke = "#009EB4";
    svgElem.style.strokeLinecap = "round";
    svgElem.style.strokeLinejoin = "round";
    svgElem.style.strokeWidth = "2";
    svgElem.style.transition = "stroke 0.2s ease-in-out 0.1s";
    svgElem.style.width = "1.5em";
    div.appendChild(svgElem);

    div.addEventListener("click", event => {
      this.toggle(useElem);
    });

    div.style.cursor = "pointer";
    parent.appendChild(div);
  }

  toggle(useElem: SVGElement) {
    this._shown = !this._shown;
    if (this._shown) {
      this.el.nativeElement.setAttribute("type", "text");
      useElem.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "assets/icons.svg#eye-off");
    } else {
      this.el.nativeElement.setAttribute("type", "password");
      useElem.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "assets/icons.svg#eye");
    }
  }
}
