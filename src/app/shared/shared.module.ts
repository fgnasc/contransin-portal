import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { FullPageWrapperComponent } from './components/full-page-wrapper/full-page-wrapper.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PasswordInputDirective } from './directives/password-input.directive';
import { NgxMaskModule } from 'ngx-mask';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BreadcrumbComponent } from './layout/breadcrumb/breadcrumb.component';
import { ToastrModule } from 'ngx-toastr';
import { ToAnyPipe } from './pipes/to-any.pipe';
import { GridComponent } from './layout/grid/grid.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ArchwizardModule } from 'angular-archwizard';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SelectableTableComponent } from './components/form/selectable-table/selectable-table.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { NgSelectModule } from '@ng-select/ng-select';
import { HeaderComponent } from './layout/header/header.component';

@NgModule({
  declarations: [
    PageNotFoundComponent,
    FullPageWrapperComponent,
    PasswordInputDirective,
    SidebarComponent,
    BreadcrumbComponent,
    ToAnyPipe,
    GridComponent,
    SelectableTableComponent,
    HeaderComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgSelectModule,
    FormsModule,
    AlertModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxMaskModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    ToastrModule.forRoot(),
    ArchwizardModule,
    MatDialogModule
  ],
  exports: [
    CommonModule,
    RouterModule,
    AlertModule,
    ToastrModule,
    TooltipModule,
    NgSelectModule,
    ArchwizardModule,
    ModalModule,
    FormsModule,
    NgxMaskModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    PageNotFoundComponent,
    FullPageWrapperComponent,
    PasswordInputDirective,
    SidebarComponent,
    BreadcrumbComponent,
    HeaderComponent,
    GridComponent,
    MatDialogModule,
    SelectableTableComponent
  ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }
  ]
})
export class SharedModule { }
